#!/usr/bin/env bash

function TEMPLATE_TO_FILE(){
    INPUTFILE=$1
    OUTPUTFILE=$2

    # check that both parameters are passed
    if [ -z $INPUTFILE ] || [ -z $OUTPUTFILE ]; then
        echo "Not all parameters passed. Command format: script input_file output_file"
        exit 1
    fi

    # check that the inputfile exists and is a regular file
    if [ ! -f $INPUTFILE ]; then
        echo "$INPUTFILE file does not exist or is not a template file."
        exit 2
    fi

    # check that outputfile does not exist
    if [ -f $OUTPUTFILE ]; then
        echo "The $OUTPUTFILE file already exists and will be overwritten."
        read -p "Continue (y/n)?" -n 1
        echo
        if  [[ ! $REPLY =~ ^[Yy]$ ]]; then
            exit 3
        else
            cp /dev/null $OUTPUTFILE
        fi
    else
        touch $OUTPUTFILE
    fi

    # read line by line the template file
    cat $INPUTFILE | while read STRING; do
        NEXT_VAR=$(echo "${STRING}" | sed -n 's/.*\({\$.*}\).*/\1/p')
        while [ "${NEXT_VAR}" ]; do
            # remove braces
            NEXT_VAR=${NEXT_VAR#"{"}
            NEXT_VAR=${NEXT_VAR%"}"}
            # get the value of the variable and insert it into the original line
            NEXT_VAR=$(eval echo $NEXT_VAR)
            STRING=$(echo "${STRING}" | sed -ne "s|\(.*\)\({\$.*}\)\(.*\)|\1${NEXT_VAR}\3|p")
            NEXT_VAR=$(echo "${STRING}" | sed -n 's/.*\({\$.*}\).*/\1/p')
        done
        echo "${STRING}" >> $OUTPUTFILE
    done
}

# check script is running itself or it is imported
if [ "$(basename $0)" == "script.sh" ]; then
    TEMPLATE_TO_FILE "$@"
fi

